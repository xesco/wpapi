function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function showplaces() {
  // clear values
  $("#loc-results").delay(500).empty();
  var val = $("#loc-filter").val();
  if (val.trim()) {
    $.get("/api/places/"+ val, function(data, status) {
      var div = $("#loc-results");
      for (i=0; i<data.length && i<=5; i++) {
        var anchor = $("<a>");
        anchor.attr("href", "#");
        anchor.attr("class", "list-group-item list-group-item-info list-group-item-action");
        anchor.attr("onclick", "setlocation('"+data[i].description+"')");
        anchor.append(document.createTextNode(data[i].description));
        div.append(anchor);
      } 
    });
  }
}

function setlocation(text) {
    $.get("/api/location/set/"+ text); 
    $("#loc-results").empty();
    $("#loc-filter").val(text);
}

$(document).ready(function() {
  // apply filter for date
  $("#date-filter").change(function() {
    var date = $("#date-filter option:selected").val();
    $("#date-filter").val(date);
    $.get("/api/filter/set/publishDate/"+ date);
  });

  // apply filter for order by
  $("#order-filter").change(function() {
    var order = $("#order-filter option:selected").val();
    $("#order-filter").val(order);
    $.get("/api/filter/set/orderBy/"+ order);
  });

  // apply filter for location
    $("#loc-filter").on("keyup", delay(showplaces, 400));
});
