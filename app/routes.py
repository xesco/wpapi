import os

from lib.api import WallaAPI
from lib.constants import order_types, last_published
from lib.decorators import session_required

from app import app
from flask import request, jsonify, session, render_template

# TODO: implement this with a Session Store
clients = {}

app.secret_key = os.urandom(24)

# Main page
@app.route('/')
def index():
    if not 'sid' in session:
        wpapi = WallaAPI()
        sid = wpapi.getSession()
        session['sid'] = sid 
        clients[sid] = wpapi

    sid = session['sid']
    data = {
      'params': clients[sid].params,
      'order': order_types,
      'date': last_published,
    }
    return render_template('index.html', **data)

@app.route('/api/search')
@session_required
def search():
    items=[]
    wpapi = clients[session['sid']]
    if request.args.get('kws', None):
        items = wpapi.search(request.args['kws'])

    data = {
      'items': items,
      'params': wpapi.params,
      'count': len(items),
      'order': order_types,
      'date': last_published,
    }
    return render_template('index.html', **data)

@app.route('/api/location/set/<placeId>')
@session_required
def setLocation(placeId):
    return jsonify(clients[session['sid']].setLocation(placeId))

@app.route('/api/location/clear')
@session_required
def clearLocation():
    jsonify(clients[session['sid']].clearLocation())
    return jsonify({'location': 'cleared'})

@app.route('/api/places/<query>')
@session_required
def places(query):
    return jsonify(clients[session['sid']].places(query))

@app.route('/api/categories/<lang>')
@session_required
def categories(lang):
    return jsonify(clients[session['sid']].categories(lang))

@app.route('/api/suggester/<query>')
@session_required
def suggester(query):
    return jsonify(clients[session['sid']].suggester(query))

@app.route('/api/publishdate/<lang>')
@session_required
def publishdate(lang):
    return jsonify(clients[session['sid']].publishdate(lang))

@app.route('/api/filter/set/<name>/<value>')
@session_required
def setFilter(name, value):
    if clients[session['sid']].setFilter(name, value):
        return jsonify({'filter_set': name})
    return jsonify({'filter': 'error'})

@app.route('/api/filter/clear/<name>')
@session_required
def clearFilter(name):
    if clients[session['sid']].clearFilter(name):
        return jsonify({'filter_cleared': name})
    return jsonify({'filter': 'error'})

