#!/bin/sh
set -e

# install requirements
pip install --upgrade pip
pip install -r requirements.txt

exec "$@"
