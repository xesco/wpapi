APP_NAME=wpapi
PORT=5555

build:
	docker build -t $(APP_NAME) .
run:
	docker run --rm -v $(PWD):/opt/wpapi -p $(PORT):$(PORT) -ti $(APP_NAME) /usr/local/bin/flask run -h 0.0.0.0 -p $(PORT)
