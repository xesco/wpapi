FROM python:3.7.2-alpine
ENV PORT 5555

VOLUME /opt/wpapi
WORKDIR /opt/wpapi

ENTRYPOINT ["/opt/wpapi/entrypoint.sh"]
