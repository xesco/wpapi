# WPAPI: Easy to use WebApp build on top of Wallapop API

## How to run
* Install python3 if you don't have it
* Clone the repo and cd into it
* Create a virtualenvironment with: pytohn3 -m venv .env
* Activate the environment with: source .env/bin/activate
* Install requirements: pip install -r requirements.txt
* Start server: flask run
* Open url http://localhost:5000

## How to run with Docker
* make build (only first time)
* make run
* Open url http://localhost:5555
