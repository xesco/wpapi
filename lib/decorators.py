from flask import session, redirect, url_for
from functools import wraps

def session_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'sid' in session:
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated_function


