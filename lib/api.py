import json
import requests

from uuid import uuid4
from configparser import ConfigParser
from lib.constants import order_types, last_published

def read_config(path):
    config = ConfigParser()
    config.read(path)
    return config

config = read_config('settings.ini')

class WallaAPI:
    protocol = config['main']['protocol']
    baseURL = config['main']['baseURL'] 
    maxItems = int(config['main']['maxItems'])

    def __init__(self):
        # core attributes
        self.paths = config['paths']
        self.session = requests.Session()
        self.cookies = {'session_id': str(uuid4())}
        self.headers = dict()
        # cached results
        self.placeId = None
        self.c_location = dict()
        self.c_places = dict()
        self.c_categories = dict()
        self.c_suggester = dict()
        self.c_publishdate = dict()
        # GET params
        self.params = {
            'dist': 400,
            'location': "",
            'publishDate': 'any',
            'kws': "",
            'order': 'distance-asc',
            'searchNextPage': None,
            'catIds': None,
            'minPrice': "",
            'maxPrice': "",
            'markAsIds': [],
        }

    def fullURL(self, endpoint):
        fullpath = "/".join([self.baseURL, self.paths[endpoint]])
        return "".join([self.protocol, "://", fullpath])

    def get(self, params, endpoint):
        data = {
            'params': params,
            'headers': self.headers,
            'cookies': self.cookies,
        }
        return self.session.get(self.fullURL(endpoint), **data).json()

    def search(self, kws):
        res = self.get({**self.params, **{'kws': kws}}, 'items')
        items = res['items']
        while res['searchNextPage'] and len(items) < self.maxItems:
            params = {**self.params, **{'kws': kws, 'searchNextPage': res['searchNextPage']}}
            res = self.get(params, 'items')
            items.extend(res['items'])
        return items

    # add more filters here
    def getFilters(self):
        def maxPrice(price):
            self.params['maxPrice'] = price

        def minPrice(price):
            self.params['minPrice'] = price

        def orderBy(order='creationDate-des'):
            if order in order_types:
                self.params['order'] = order

        def publishDate(last='any'):
            if last in last_published:
                self.params['publishDate'] = last

        def distance(dist='400'):
            if 1 <= int(dist) <= 400:
                self.params['dist'] = dist

        def markAsIds(mark):
            pass

        return {
            maxPrice.__name__: maxPrice,
            minPrice.__name__: minPrice,
            orderBy.__name__: orderBy,
            publishDate.__name__: publishDate,
        }

    def setFilter(self, name, value):
        filters = self.getFilters()
        if name in filters:
            filters[name](value)
            return True
        return False
    
    def clearFilter(self, name):
        filters = self.getFilters()
        if name in filters:
            self.params[name] = None
            return True
        if name == 'all':
            for name in filters.keys():
                self.params[name] = None
            return True
        return False

    def places(self, query):
        if not self.c_places.get(query):
            self.c_places[query] = self.get({'query': query}, 'places')
        return self.c_places[query]

    def setLocation(self, placeId):
        if not self.c_location.get(placeId):
            self.placeId = self.params['location'] = placeId
            self.c_location[placeId] = self.get({'placeId': placeId}, 'location')
            self.cookies['searchLat'] = str(self.c_location[placeId]['latitude'])
            self.cookies['searchLng'] = str(self.c_location[placeId]['longitude'])
        return self.c_location[placeId]

    def clearLocation(self):
        if self.placeId:
            self.params['location'] = ""
            self.c_location[self.placeId] = None
            self.placeId = None
            self.cookies['searchLat'] = None
            self.cookies['searchLng'] = None

    def categories(self, language='en'):
        if not self.c_categories.get(language):
            self.c_categories[language] = self.get({'language': language}, 'categories')
        return self.c_categories[language]

    def suggester(self, keyword):
        if not self.c_suggester.get(keyword):
            self.c_suggester[keyword] = self.get({'keyword': keyword}, 'suggester')
        return self.c_suggester[keyword]

    def publishdate(self, language='en'):
        if not self.c_publishdate.get(language):
            self.c_publishdate[language] = self.get({'language': language}, 'publishdate')
        return self.c_publishdate[language]

    def getSession(self):
        return self.cookies['session_id']
