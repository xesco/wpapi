order_types = {
    'salePrice-asc': 'price asc',
    'salePrice-desc': 'price desc',
    'distance-asc': 'distance asc',
    'creationDate-des': 'date desc',
}

last_published = {
    '24': 'last 24h',
    '7': 'last 7 days',
    '30': 'last month',
    'any': 'anytime',
}
